# coding=utf-8
# Caleb Freeman
# 8/21/2019
# CS 4500
# ###explanation of what the program is designed to do###
# This program is designed to play a connected diagraph game:
#   1) Place a magnetic marker in circle #1, and put a check mark in circle #1.
#   The circle where the marker resides is called the “current circle.”
#   2) Randomly choose among the out arrows in the current circle.
#   (If there is only one out arrow that choice is trivial) In this selection, all the out arrows should
#   be equally likely to be picked.
#   3) Move the marker to the circle pointed to by the out arrow. This becomes the new current circle.
#   4) Put a check mark in the current circle.
#   5) If all the circles have at least one check mark, stop the game. If not, go to step 2 and repeat.
# The central data structures used are pythons lists implementation
# External input file HW2infile.txt that consists of:
#   The first line has only the number N, the number of circles that will be used in your game. max of 20
#   The second line has the number K, the number of arrows you will “drawing” between the circles. max of 20
#   The next K lines designate the arrows, one arrow per line.
#   Each arrow line consists of two numbers, each number being
#   one of circles in the game. These two numbers are separated by a single blank.
#   The first number designates the circle
#   that is the source (back end) of the arrow; the second number designates the circle that is the destination
#   (pointed end) of the arrow.
# ### HOMEWORK 2 AMENDMENTS ###
# added additional list to keep track of the number of checks performed on per a circle across all game simulations
# changed the output file, and contents of what is printed to the new output file to contain the data of the new list
# checksPerSingleCircle and checksPerCircleSumOfGames
import random  # for selecting randomly which outArrow to follow

with open('HW2freemanOutfile.txt', 'w') as the_file:
    print()
# read input file named HW2infile.txt
with open('HW2infile.txt') as f:
    dataArray = []
    lines = f.read().split()
    try:
        for line in lines:
            dataArray.append(int(line))
    except:
        print("sorry input file must contain only integers")
        # writing error output to output file
        with open('HW2freemanOutfile.txt', 'w') as the_file:
            the_file.write('Exception: Input file must only contain integers\n')
        exit(1)

# line one the number of circles must be a single integer N between 2 and 20
try:
    if isinstance(dataArray[0], int) and (2 <= dataArray[0] <= 20):
        print("")
    else:
        raise ValueError
except ValueError:
    print("Sorry the infile first line must be a single integer between 2 and 10 ")
    with open('HW2freemanOutfile.txt', 'w') as the_file:
        the_file.write('Exception: First line of input file must contain a single integer between 2 and 10\n')
    exit(1)

# line two is a single integer K that is equal to the number of arrows
# (length of dataArray - 2 / 2 ) or the length of the array minus the two K and N inputs
# divided by two, for the input and output for each arrow, will equal K
try:
    # comparing k, the number of arrows is
    k = dataArray[1]
    if ((len(dataArray) - 2) / 2) != dataArray[1]:
        raise ValueError
except ValueError:
    print("Sorry, the second line of the input must contain a single integer K equal to the number of total arrows.")
    with open('HW2freemanOutfile.txt', 'w') as the_file:
        the_file.write('Exception: Second line of input must contain a single integer K equal to the number arrows\n')
    exit(1)

# variables below for homework 2 criteria
gameSimulations = 10
totalGameSims = gameSimulations
totalChecks = []  # list for storing the total checks per game over multiple simulations
#  checksEachGame = []  # number of total checks per each game reimplemented
checksPerSingleCircle = []  # list of checks performed per circle over all games
checksPerCircleSumOfGames = []  # for all games

while gameSimulations != 0:

    # playing the GAME
    # POPULATING list to check if the circles have been visited, and to hold the arrow coordinates between circles
    n = dataArray[0]
    outArrowsStartingIndex = []
    outArrowEndingIndex = []
    gameCheck = []
    gameCheckStrength = []
    # populate an list with of size N, will remove a value each time
    for i in range(n):
        gameCheck.append(1)
        gameCheckStrength.append(1)

    # feeding the first coordinate of arrows pairs into an array will have matching indices with outArrowEndingIndex
    for i in range(2, len(dataArray), 2):
        outArrowsStartingIndex.append(dataArray[i])

    # feeding the second coordinate of arrows pairs into an array will have matching indices with outArrowStatingIndex
    for i in range(3, len(dataArray), 2):
        outArrowEndingIndex.append(dataArray[i])
    # HOMEWORK 2 REQUIREMENT CHECKING IF INPUT IS STRONGLY CONNECTED
    # https://thispointer.com/python-check-if-a-list-contains-all-the-elements-of-another-list/
    # checks if every element in startingArrows are in EndingArrows, and
    # checks if every element in EndingArrows are in StartingArrows

    # copy the lists to new arrays for sorting and comparing for identical value
    startingList = outArrowsStartingIndex.copy()
    endingList = outArrowEndingIndex.copy()

    # removing duplicates from lists by converting to dictionaries and back to lists
    startingList = list(dict.fromkeys(startingList))
    endingList = list(dict.fromkeys(endingList))

    # sorting lists
    startingList.sort()
    endingList.sort()

    # if the lists are equal they are strongly connected diagraphs
    if startingList != endingList:
        print("no no no, absolutely not! a strongly connected diagraph input")
        with open('HW2freemanOutfile.txt', 'w') as the_file:
            the_file.write('Exception: HW2infile.txt must contain a strongly connected diagraph')
        exit(1)
    # above solution comparing startingList and endingList does not cover all solutions.

    checksStrength = 0 # variable for counting max checks, in checking if stongly connected outside of starting index loop

    for i in range(1, n):
        currentStr = i
        listOfOutArrowsStrength = []
        for j in range(len(gameCheckStrength)):
            gameCheckStrength[j] = 1
        gameCheckStrength[currentStr] = 0  # start the game by checking the first circle as visisted
        outArrowSearch = currentStr  # index of the current circle
        while sum(gameCheckStrength) != 0: # find the max value of the array
            # for loop that searches for index values of arrows that are connected
            outArrowSearch = currentStr
            for i in range(len(outArrowsStartingIndex)):
                if outArrowsStartingIndex[i] == outArrowSearch:  # append a list of arrow index leaving the current circle
                    listOfOutArrowsStrength.append(i)
            if len(listOfOutArrowsStrength) != 0:
                randomArrowChoice = random.choice(listOfOutArrowsStrength)  # rand select index for an output arrow from current circle
            else:
                print("not strong!")
                exit(1)
            currentStr = outArrowEndingIndex[randomArrowChoice]  # current index is always one less ths line needs testing!!!
            listOfOutArrowsStrength.clear()  # clears the list of possible out arrow choices after selecting one randomly

            if currentStr <= len(gameCheckStrength):  # bounds checking before marking a circle as visited
                solIndex = outArrowEndingIndex[randomArrowChoice]
                if gameCheckStrength[solIndex - 1] != 0:  # list of sol. indices are off by one b/c circle count does not start at 0
                    gameCheckStrength[solIndex - 1] = 0
                else:
                    checksStrength = checksStrength + 1
                    if checksStrength > 1000000:
                        print("NOT STRONG.")
                        exit(1)
            else:
                continue

    # game starts with the first circle being filled
    # placeholder for position in the game, we start at the first position
    # odd numbers in dataArray[3] ... dataArray[i] are circle starting positions.
    # variables below for game play
    listOfOutArrows = []  # list of out arrows to randomly select rom
    current = 0  # index of current arrow being compared to
    gameCheck[current] = 0  # starts the game by checking the first circle as visited
    current = 1  # increments the current circle

    # list for finding max checks per a circle in homework 2
    checksPerCircle = []
    for i in range(len(gameCheck)):
        checksPerCircle.append(1)
    numberOfChecks = 0
    # while loop to play the game
    while sum(gameCheck) != 0:
        outArrowSearch = current  # index of the current circle
        # find the max value of the array

        # for loop that searches for index values of arrows that are connected
        for i in range(len(outArrowsStartingIndex)):
            if outArrowsStartingIndex[i] == outArrowSearch:  # append a list of arrow index leaving the current circle
                listOfOutArrows.append(i)
        randomArrowChoice = random.choice(listOfOutArrows)  # rand select index for an output arrow from current circle
        current = outArrowEndingIndex[randomArrowChoice]  # current index is always one less ths line needs testing!!!
        listOfOutArrows.clear()  # clears the list of possible out arrow choices after selecting one randomly

        if current <= len(gameCheck):  # bounds checking before marking a circle as visited
            solIndex = outArrowEndingIndex[randomArrowChoice]
            if gameCheck[solIndex - 1] != 0:  # list of sol. indices are off by one b/c circle count does not start at 0
                gameCheck[solIndex - 1] = 0
            else:
                checksPerCircle[current] = checksPerCircle[current] + 1
                if sum(checksPerCircle) > 1000000:
                    print("The game has exceeded the allowed checks per a single game of 1000000, now EXITING.")
                    exit(1)
        else:
            continue

    totalChecks.append(sum(checksPerCircle))
    gameSimulations = gameSimulations - 1
    checksPerCircleSumOfGames.extend(checksPerCircle)
    # output criteria
    print("The number of circles that were used for this game: ", n)
    print("The number of arrows that were used for this game: ", k)
    print("The total number of checks on all the circles combined: ", (sum(checksPerCircle)))  # plus one for the first check
    print("The average number of checks in a circle marked during the game: ", sum(checksPerCircle) / n)
    print("The maximum number of checks in any one circle: ", max(checksPerCircle))

    with open('HW2freemanOutfile.txt', 'a') as the_file:
        the_file.write("The number of circles that were used for this game: " + str(n) + "\n")
        the_file.write("The number of arrows that were used for this game: " + str(k) + "\n")
        the_file.write("The total number of checks on all the circles combined: " + str(sum(checksPerCircle)) + "\n")
        the_file.write("The average number of checks in a circle marked during the game: " + str(sum(checksPerCircle) / n) + "\n")
        the_file.write("The maximum number of checks in any one circle: " + str(max(checksPerCircle)) + "\n")
        the_file.write("\n")

avgTotalChecks = sum(totalChecks) / 10

# finding the average number o checks
avgSingleCircleChecksOverAll = (sum(checksPerCircleSumOfGames) / (n * totalGameSims))



# output criteria for homework2
print("The average number of total checks per game over 10 games: ", avgTotalChecks)
print("The maximum number of total checks in a single game: ", max(totalChecks))
print("The minimum number of total checks in a single game: ", min(totalChecks))
print("The average number of checks on a single circle over all the games: ", avgSingleCircleChecksOverAll)
print("The minimum number of single circle checks: ", max(checksPerCircleSumOfGames))
print("The maximum number of single circe checks: ", min(checksPerCircleSumOfGames))

with open('HW2freemanOutfile.txt', 'a') as the_file:
    the_file.write("The average number of total checks per game over 10 games: " + str(avgTotalChecks) + "\n")
    the_file.write("The maximum number of total checks in a single game: " + str(max(totalChecks)) + "\n")
    the_file.write("The minimum number of total checks in a single game: " + str(min(totalChecks)) + "\n")
    the_file.write("The average number of checks on a single circle over all the games: "
                   + str(avgSingleCircleChecksOverAll) + "\n")
    the_file.write("The minimum number of single circle checks: " + str(max(checksPerCircleSumOfGames)) + "\n")
    the_file.write("The maximum number of single circe checks: " + str(min(checksPerCircleSumOfGames)) + "\n")

exit(0)
